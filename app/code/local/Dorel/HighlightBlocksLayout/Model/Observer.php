<?php
class Dorel_HighlightBlocksLayout_Model_Observer extends Varien_Object{
    const FLAG_ACTIVATED = showBlocks;

    public function afterHtml($observer) {
        if($this->isActivated()) {
            $transport          = $observer->getTransport();
            $html = $transport->getHtml();
            /* @var $block Mage_Core_Block_Abstract */
            $block  = $observer->getBlock();

            $preHtml    = '<div class="blockwrapper"><div class="blockname">'.$block->getNameInLayout().'</div>';
            $postHtml   = '</div>';

            $html = $preHtml . $html . $postHtml;
            $transport->setHtml($html);
        }
    }

    private function isActivated() {
        $is_set = array_key_exists(self::FLAG_ACTIVATED,$_GET);
        if($is_set && !empty($_GET[self::FLAG_ACTIVATED])) {
            return true;
        }
        return false;
    }

}